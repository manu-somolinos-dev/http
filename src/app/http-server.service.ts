import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {catchError, map} from 'rxjs/operators';
import {_catch} from '../../node_modules/rxjs-compat/operator/catch';
import {throwError} from 'rxjs';

@Injectable()
export class HttpServerService {

  constructor(private http: Http) { }

  storeServers(servers: any []) {
    const headers = new Headers({
      'content-type': 'application/json'
    });
    return this.http.post('http://localhost:9090/api/servers/all', servers, {
      headers: headers
    });
  }

  putServer(server: any) {
    const headers = new Headers({
      'content-type': 'application/json'
    });
    return this.http.put('http://localhost:9090/api/servers', server, {
      headers: headers
    });
  }

  getServers() {
    return this.http
      .get('http://localhost:9090/api/servers')
      .pipe(
        map( (response) => {
          const data = response.json();
          data.forEach(server => {
            server.name = 'Fetched_' + server.name;
          });
          return data;
        })
      );
  }

  getOneServer(id: number) {
    return this.http.get('http://localhost:9090/api/servers/' + id)
      .pipe(
        map( (response) => response.json())
      )
      .pipe(
        catchError(error => throwError(error))
      );
  }

  getHealthStatus() {
    return this.http
      .get('http://localhost:9090/api/health')
      .pipe(
        map( (response) => response.json().status )
      );
  }
}
