import {Component, OnInit} from '@angular/core';
import {HttpServerService} from './http-server.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  servers = [
    {
      name: 'Testserver',
      capacity: 10,
      id: this.generateId()
    },
    {
      name: 'Liveserver',
      capacity: 100,
      id: this.generateId()
    }
  ];

  appHealthStatus = this.httpServerService.getHealthStatus()

  constructor(private httpServerService: HttpServerService) { }

  onAddServer(name: string) {
    this.servers.push({
      name: name,
      capacity: 50,
      id: this.generateId()
    });
  }
  private generateId() {
    return Math.round(Math.random() * 10000);
  }

  onSave() {
    this.httpServerService.storeServers(this.servers).subscribe(
      (response) => console.log('success', response),
      (error) => console.log('error', error)
    );
  }

  onGet() {
    this.httpServerService.getServers().subscribe(
      (servers: any []) => this.servers = servers,
      (error) => console.log('error', error)
    );
  }

  onGetOneInvalid() {
    this.httpServerService.getOneServer(-9999).subscribe(
      (server: any) => this.servers.push(server),
      (error) => console.log('error Get One Invalid', error)
    );
  }

  onPut() {
    const server = this.servers[0];
    server.name = 'Edit Name';
    server.capacity = 81;
    this.httpServerService.putServer(server).subscribe(
      (response) => console.log('success', response),
      (error) => console.log('error', error)
    );
  }
}
